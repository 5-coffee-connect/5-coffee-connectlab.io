---
hero_heading: 5
hero_subheading: Coffee Connect
about_title: A bit about us
about_content: <p>5, Coffee Connect Amsterdam is a Community Centre and Coffee bar
  in the Asylum Seekers Centre (AZC) Willinklaan in Amsterdam.</p><p>The cafe is run
  by members of the AZC Community, who’ve been trained as baristas. They’re here to
  serve some great, affordable coffee with excellent hospitality to boot (Psst, check
  out our <a href="#menu" title="Check out our menu">menu</a>).</p><p>Our goal was
  to create a place where newcomers can find a place of belonging; where they can
  learn new skills and hone their existing ones. At the same time, this is a space
  for our Nieuw-West neighbourhood to come together (and maybe learn something new
  from our community).</p><p>Our venue is perfect for social and work meetings alike.
  Looking to rent the space? We’re free to rent most evenings and weekdays. Just email
  us to enquire about availability. To see some examples of events we’ve had in-house,
  just check out our <a href="https://www.facebook.com/" title="Check out our Facebook
  page">Facebook</a> or <a href="https://www.instagram.com/?hl=en" title="Check out
  our Instagram page">Instagram</a> pages.</p>
layout: page
video: https://www.w3schools.com/html/mov_bbb.mp4
images:
- "/uploads/Foto 04-04-18 13 03 01-1.jpg"
- "/uploads/Foto 15-05-18 19 06 53.jpg"
- "/uploads/Foto 06-04-18 13 41 29.jpg"
date: 2019-03-11 08:33:18 +0000
title: 5 Coffee Connect
footer_contact: '<p>Willinklaan 3<br>1067 SL Amsterdam<br>Opening hours: 10:00 – 18:00
  Tuesday – Friday<br><a href="mailto:5@eigenwijks.nl" title="Email us to 5@eigenwijks.nl">5@eigenwijks.nl</a></p>'
list_item:
- list_item_name: Cappuccino
  list_item_price: '3.50'
- list_item_name: Espresso
  list_item_price: '2.50'
list:
- item:
  - {}
lists:
- items:
  - name: Americano
    price: '1'
  - name: Espresso
    price: '0.50'
  - name: Double espresso
    price: '1'
  - price: '1.50'
    name: Cappuccino
  - name: Latte
    price: '2'
  - name: Hot Chocolate
    price: '1.50'
  - name: Tea
    price: '1'
  list_title: Hot drinks
  title: Hot drinks
- items:
  - name: Orange juice
    price: '1.50'
  list_title: Cold drinks
  title: Cold drinks

---
